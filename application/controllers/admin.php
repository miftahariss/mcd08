<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	
	public function index()
	{
		if($this->session->userdata('loginAdminKidnesiamcdFutsal2018')!="")
		{
			redirect('/main');
		}
		else
		{
			$data['title'] = "";
			$this->load->view('backend/login',$data);
		}
	}

}
