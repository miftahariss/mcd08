<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	
	
	
	public function index()
	{
		$data = array();
		$this->form_validation->set_rules('username', 'Username', 'required'); //TEKS
		$this->form_validation->set_rules('password', 'Password', 'required'); //TEKS

		if ($this->form_validation->run() == TRUE)
		{
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			if($username=="admin" && $password=="mcd2018")
			{
				$remember_me = $this->input->post('remember_me', TRUE);
		        if($remember_me=="on")
		        {
		        	
		            if(isset($_COOKIE["usernamemcd2018"]))
		            {
		                unset($_COOKIE['usernamemcd2018']);
		            }
		            if(isset($_COOKIE["passwordmcd2018"]))
		            {
		                unset($_COOKIE['passwordmcd2018']);
		            }
		            setcookie("usernamemcd2018",$username, time() + (10 * 365 * 24 * 60 * 60));
		            setcookie("passwordmcd2018",$password, time() + (10 * 365 * 24 * 60 * 60));
		        }

				$userdata = array(
					'loginAdminKidnesiamcdFutsal2018' => '1'
				);
					
				$this->session->set_userdata($userdata);
				redirect('main');
			}
			else
			{
				$this->session->set_flashdata('message_warning', 'Login Gagal');
				redirect('admin');
			}
		}
		else
		{
			$this->session->set_flashdata('message_warning', 'Login Gagal');
			redirect('admin');
		}
	}
}
