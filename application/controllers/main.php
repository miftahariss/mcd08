<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Main extends CI_Controller {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	
	function __construct() 
	{
        parent::__construct();
        $this->load->model('frontend_model');
		$this->load->library('form_validation');

		
    }
	
	public function index()
	{
		if(!$this->session->userdata('loginAdminKidnesiamcdFutsal2018'))
		{
			redirect('admin');
		}
		else
		{
			$data['hari'] = $this->frontend_model->hariIndonesia(date('l'));
			$data['tanggal'] = $this->frontend_model->lihatHari(date('Y-m-d'));
			$data['ucapan'] = $this->frontend_model->ucapan();

			$data['title'] = "Admin : Mcd Futsal Championship 2018";
			$this->load->view('backend/template',$data);
		}
	}

	public function dashboard()
    {
    	ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);

        $data['dari'] = $this->uri->segment(3);
        $data['sampai'] = $this->uri->segment(4);
        $cekgarismiring = substr($data['dari'],2,1);
        if($cekgarismiring=="-")
        {
            $data['dari'] = str_replace('-','/',$data['dari']);
            $data['sampai'] = str_replace('-','/',$data['sampai']);
        }


        $data['title'] = 'Admin : Mcd Futsal Championship 2018';
        $data['mainpage'] = "backend/dashboard";
        $this->load->view('backend/template_tabs', $data);
    }

}
