<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Member extends CI_Controller {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	
	function __construct() {
        parent::__construct();
        $this->load->model('frontend_model');
		$this->load->library('form_validation');

		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
    }

    
    public function detailuserfoto($id)
    {
    	if(!$this->session->userdata('loginAdminKidnesiamcdFutsal2018'))
		{
			echo "
			<script>
			parent.redirectMe();
			</script>
			";
		}
		else
		{
			$data['user'] = $this->frontend_model->dataUserDetailFoto($id);
    		
    		$data['title'] = "DETAIL USER PHOTO CONTEST";
			$data['mainpage'] = 'backend/detailuserfoto';
			$this->load->view('backend/template_tabs',$data);
    	}
    }

    public function detailuser($id)
    {
    	if(!$this->session->userdata('loginAdminKidnesiamcdFutsal2018'))
		{
			echo "
			<script>
			parent.redirectMe();
			</script>
			";
		}
		else
		{
			$data['user'] = $this->frontend_model->dataUserDetail($id);
    		
    		$data['title'] = "DETAIL USER";
			$data['mainpage'] = 'backend/detailuser';
			$this->load->view('backend/template_tabs',$data);
    	}
    }

    public function hapuskuis($id)
    {
    	if(!$this->session->userdata('loginAdminKidnesiamcdFutsal2018'))
		{
			echo "
			<script>
			parent.redirectMe();
			</script>
			";
		}
		else
		{
	    	$data_update = array('sts_rc' => '0',);
			$this->db->where('id', $id);
			$update = $this->db->update('kidnesia_mcd2018_kuis_member', $data_update);

			$this->session->set_flashdata('message_success', 'Peserta Kuis Berhasil Dihapus');
	    	redirect('member/datamemberkuis/all');
    	}
    }

    public function hapuspuzzle($id)
    {
    	if(!$this->session->userdata('loginAdminKidnesiamcdFutsal2018'))
		{
			echo "
			<script>
			parent.redirectMe();
			</script>
			";
		}
		else
		{
	    	$data_update = array('sts_rc' => '0',);
			$this->db->where('id', $id);
			$update = $this->db->update('kidnesia_mcd2018_puzzle_member', $data_update);

			$this->session->set_flashdata('message_success', 'Peserta Puzzle Berhasil Dihapus');
	    	redirect('member/datamemberpuzzle/all');
    	}
    }

    
    public function hapusfoto($id,$sts)
    {
    	if(!$this->session->userdata('loginAdminKidnesiamcdFutsal2018'))
		{
			echo "
			<script>
			parent.redirectMe();
			</script>
			";
		}
		else
		{
			if($sts==1)
			{
				$this->session->set_flashdata('message_success', 'Photo Tidak Ditampilkan');
				$data_update = array('sts_rc' => '0',);
			}
			elseif($sts==0)
			{
				$this->session->set_flashdata('message_success', 'Photo Ditampilkan');
				$data_update = array('sts_rc' => '1',);
			}

	    	
			$this->db->where('id', $id);
			$update = $this->db->update('kidnesia_mcd2018_twitPic', $data_update);

			
	    	redirect('member/datamemberfoto/all');
    	}
    }

	public function hapus($id,$lomba)
    {
    	if(!$this->session->userdata('loginAdminKidnesiamcdFutsal2018'))
		{
			echo "
			<script>
			parent.redirectMe();
			</script>
			";
		}
		else
		{
	    	$data_update = array('sts_rc' => '0',);
			$this->db->where('id', $id);
			$update = $this->db->update('kidnesia_mcd2018_peserta', $data_update);

			$this->session->set_flashdata('message_success', 'Peserta Berhasil Dihapus');
	    	redirect('member/datamember/all');
    	}
    }

    public function datamemberkuis()
    {
    	if(!$this->session->userdata('loginAdminKidnesiamcdFutsal2018'))
		{
			echo "
			<script>
			parent.redirectMe();
			</script>
			";
		}
		else
		{
			$limit = $this->uri->segment(4);
			if($limit=="")
			{
				$limit = 0;
			}

			$config['per_page'] = 10;
			$data['currentPage'] = $limit;

			$config['uri_segment'] = 4;

			$key = urldecode($this->uri->segment(3));

			$keyUrl = $key;

			if($keyUrl=="all")
			{
				$key = "";
			}

			$data['key'] = $keyUrl;
			
			$config['total_rows'] = $this->frontend_model->total_member_kuis($key);
			$data['list_member_kuis'] = $this->frontend_model->listMember_kuis($config['per_page'], $limit,$key);
			$config['base_url'] = site_url('member/datamemberkuis/'.$keyUrl);

        	$this->load->library('pagination');
	        $config['first_tag_open'] = "<span class='next'>";
	        $config['first_tag_close'] = "</span>";
	        $config['cur_tag_open'] = "<span class='current'>";
	        $config['cur_tag_close'] = "</span>";
	        $this->pagination->initialize($config);

			$data['title'] = "MEMBER";
			$data['mainpage'] = 'backend/memberkuis';
			$this->load->view('backend/template_tabs',$data);
		}
    }

    public function datamemberpuzzle()
    {
    	if(!$this->session->userdata('loginAdminKidnesiamcdFutsal2018'))
		{
			echo "
			<script>
			parent.redirectMe();
			</script>
			";
		}
		else
		{
			$limit = $this->uri->segment(4);
			if($limit=="")
			{
				$limit = 0;
			}

			$config['per_page'] = 10;
			$data['currentPage'] = $limit;

			$config['uri_segment'] = 4;

			$key = urldecode($this->uri->segment(3));

			$keyUrl = $key;

			if($keyUrl=="all")
			{
				$key = "";
			}

			$data['key'] = $keyUrl;
			
			$config['total_rows'] = $this->frontend_model->total_member_puzzle($key);
			$data['list_member_puzzle'] = $this->frontend_model->listMember_puzzle($config['per_page'], $limit,$key);
			$config['base_url'] = site_url('member/datamemberpuzzle/'.$keyUrl);

        	$this->load->library('pagination');
	        $config['first_tag_open'] = "<span class='next'>";
	        $config['first_tag_close'] = "</span>";
	        $config['cur_tag_open'] = "<span class='current'>";
	        $config['cur_tag_close'] = "</span>";
	        $this->pagination->initialize($config);

			$data['title'] = "MEMBER";
			$data['mainpage'] = 'backend/memberpuzzle';
			$this->load->view('backend/template_tabs',$data);
		}
    }

    public function datamemberfoto()
    {
    	if(!$this->session->userdata('loginAdminKidnesiamcdFutsal2018'))
		{
			echo "
			<script>
			parent.redirectMe();
			</script>
			";
		}
		else
		{
			$limit = $this->uri->segment(4);
			if($limit=="")
			{
				$limit = 0;
			}

			$config['per_page'] = 10;
			$data['currentPage'] = $limit;

			$config['uri_segment'] = 4;

			$key = urldecode($this->uri->segment(3));

			$keyUrl = $key;

			if($keyUrl=="all")
			{
				$key = "";
			}

			$data['key'] = $keyUrl;
			
			$config['total_rows'] = $this->frontend_model->total_member_foto($key);
			$data['list_member'] = $this->frontend_model->listMember_foto($config['per_page'], $limit,$key);
			$config['base_url'] = site_url('member/datamemberfoto/'.$keyUrl);

        	$this->load->library('pagination');
	        $config['first_tag_open'] = "<span class='next'>";
	        $config['first_tag_close'] = "</span>";
	        $config['cur_tag_open'] = "<span class='current'>";
	        $config['cur_tag_close'] = "</span>";
	        $this->pagination->initialize($config);

			$data['title'] = "MEMBER";
			$data['mainpage'] = 'backend/member_foto';
			$this->load->view('backend/template_tabs',$data);

		}
    }
    
	public function datamember()
	{
		if(!$this->session->userdata('loginAdminKidnesiamcdFutsal2018'))
		{
			echo "
			<script>
			parent.redirectMe();
			</script>
			";
		}
		else
		{
			$limit = $this->uri->segment(4);
			if($limit=="")
			{
				$limit = 0;
			}

			$config['per_page'] = 10;
			$data['currentPage'] = $limit;

			$config['uri_segment'] = 4;

			$key = urldecode($this->uri->segment(3));

			$keyUrl = $key;

			if($keyUrl=="all")
			{
				$key = "";
			}

			$data['key'] = $keyUrl;
			
			$config['total_rows'] = $this->frontend_model->total_member($key);
			$data['list_member'] = $this->frontend_model->listMember($config['per_page'], $limit,$key);
			$config['base_url'] = site_url('member/datamember/'.$keyUrl);

        	$this->load->library('pagination');
	        $config['first_tag_open'] = "<span class='next'>";
	        $config['first_tag_close'] = "</span>";
	        $config['cur_tag_open'] = "<span class='current'>";
	        $config['cur_tag_close'] = "</span>";
	        $this->pagination->initialize($config);

			$data['title'] = "MEMBER";
			$data['mainpage'] = 'backend/member';
			$this->load->view('backend/template_tabs',$data);

		}
	}

	public function downloadExcel($act)
	{
		if($act=='futsal')
		{
			$data['list_member'] = $this->frontend_model->listMemberExcel();
		}
		elseif($act=='kuis')
		{
			$data['list_member'] = $this->frontend_model->listMember_kuis_excel();
		}
		elseif ($act=='puzzle')
		{
			$data['list_member'] = $this->frontend_model->listMember_puzzle_excel();
		}
		elseif($act=='foto')
		{
			$data['list_member'] = $this->frontend_model->listMember_foto_excel();
		}

		
		$data['act'] = $act;
		$data['mainpage'] = 'backend/downloadExcel';
		$this->load->view('backend/template_tabs',$data);
	}

}
