<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Video extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('frontend_model');
		$this->load->library('form_validation');

		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
    }

    public function datavideo()
	{
		if(!$this->session->userdata('loginAdminKidnesiamcdFutsal2018'))
		{
			echo "
			<script>
			parent.redirectMe();
			</script>
			";
		}
		else
		{
			$data['list_video'] = $this->frontend_model->listVideo();

			$data['title'] = "VIDEO";
			$data['mainpage'] = 'backend/video';
			$this->load->view('backend/template_tabs',$data);
		}
	}

    public function addVideo(){
        if(!$this->session->userdata('loginAdminKidnesiamcdFutsal2018'))
        {
            echo "
            <script>
            parent.redirectMe();
            </script>
            ";
        }
        else
        {
            if($this->input->post('submit')){
                $valid = $this->form_validation;
                $valid->set_rules('video_url', 'Video URL', 'required');
                $valid->set_rules('caption', 'Caption', 'required');

                if ($valid->run() == false) {
                    // run
                } else {
                    $format_upload = $this->upload();
                    $data = array(
                        'video_url' => $this->input->post('video_url'),
                        'caption' => $this->input->post('caption'),
                        'is_home' => $this->input->post('is_home') ? 1 : 0,
                        'thumbnail' => $format_upload,
                        'created_at' => date('Y-m-d H:i:s'),
                        'status' => 1
                    );

                    $this->db->insert('kidnesia_mcd2018_video', $data);

                    $this->session->set_flashdata('message_success', 'Video Berhasil Disubmit');
                    redirect('video/datavideo/all');
                }
            }
            //$data['user'] = $this->frontend_model->dataArtikelDetail($id);
            
            $data['title'] = "TAMBAH VIDEO";
            $data['mainpage'] = 'backend/addvideo';
            $this->load->view('backend/template_tabs',$data);
        }
    }

	public function editvideo($id){
    	if(!$this->session->userdata('loginAdminKidnesiamcdFutsal2018'))
		{
			echo "
			<script>
			parent.redirectMe();
			</script>
			";
		}
		else
		{
			if ($this->input->post('submit')) {
				$valid = $this->form_validation;
				$valid->set_rules('video_url', 'Video URL', 'required');
                $valid->set_rules('caption', 'Caption', 'required');

                if ($valid->run() == false) {
                    // run
                } else {
                	$format_upload = $this->upload();
                    if ($format_upload != "") {
                    	$data = array(
	                		'video_url' => $this->input->post('video_url'),
	                		'caption' => $this->input->post('caption'),
                            'is_home' => $this->input->post('is_home') ? 1 : 0,
	                		'thumbnail' => $format_upload,
	                		'modified_at' => date('Y-m-d H:i:s')
	                	);

	                	$this->db->where('id', $id);
	    				$this->db->update('kidnesia_mcd2018_video', $data);
                    } else {
                    	$data = array(
	                		'video_url' => $this->input->post('video_url'),
	                		'caption' => $this->input->post('caption'),
                            'is_home' => $this->input->post('is_home') ? 1 : 0,
	                		'modified_at' => date('Y-m-d H:i:s')
	                	);

	                	$this->db->where('id', $id);
	    				$this->db->update('kidnesia_mcd2018_video', $data);
                    }

                    $this->session->set_flashdata('message_success', 'Video Berhasil Diedit');
	    			redirect('video/datavideo');
                }
			}

			$data['video'] = $this->frontend_model->dataVideoDetail($id);
    		
    		$data['title'] = "EDIT VIDEO";
			$data['mainpage'] = 'backend/editvideo';
			$this->load->view('backend/template_tabs',$data);
    	}
    }

    public function hapus($id)
    {
        if(!$this->session->userdata('loginAdminKidnesiamcdFutsal2018'))
        {
            echo "
            <script>
            parent.redirectMe();
            </script>
            ";
        }
        else
        {
            $data_update = array('status' => '0',);
            $this->db->where('id', $id);
            $update = $this->db->update('kidnesia_mcd2018_video', $data_update);

            $this->session->set_flashdata('message_success', 'Video Berhasil Dihapus');
            redirect('video/datavideo/all');
        }
    }

    public function downloadExcel($act)
    {
        $data['list_member'] = $this->frontend_model->listVideoExcel();

        
        $data['act'] = $act;
        $data['mainpage'] = 'backend/downloadExcel';
        $this->load->view('backend/template_tabs',$data);
    }

    /**
     * Upload images
     * @return string
     */
    private function upload() {
        $this->load->library('image_lib');
        $format_upload = '';
        $rename = url_title(time());
        if (isset($_FILES['userfile']['name']) && $_FILES['userfile']['name'] != "") {

            $base_path = APPPATH . '../uploads/images/';
            //chmod($base_path, 0777);
            $ori_path = $base_path . 'original/';

            $size = array(
                array('width' => '300', 'height' => '300', 'type' => 'small'),
                array('width' => '600', 'height' => '300', 'type' => 'medium'),
                array('width' => '1440', 'height' => '580', 'type' => 'large'),
            );

            //UPLOAD ORG IMAGE
            $config = array(
                'upload_path' => $ori_path,
                'allowed_types' => 'gif|jpg|jpeg|png',
                'max_size' => '5048'
            );
            $this->load->library('upload', $config);
            $this->upload->do_upload();

            foreach ($size as $value) {

                $image_data = $this->upload->data();

                //RESIZE IMAGE
                $config_thumb = array(
                    'image_library' => 'gd2',
                    'source_image' => $image_data['full_path'],
                    'new_image' => $base_path . $value["type"],
                    'create_thumb' => false,
                    'maintain_ratio' => true,
                    'width' => $value['width'],
                    'height' => $value['height'],
                    'quality' => '100%'
                );

                $dim = (intval($image_data["image_width"]) / intval($image_data["image_height"])) - ($config_thumb['width'] / $config_thumb['height']);
                $config_thumb['master_dim'] = ($dim > 0)? "height" : "width";

                $this->image_lib->initialize($config_thumb);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                //CROPING
                switch ($value['type']) {
                    case 'small':
                        $meta_image['small'] = $base_path . 'small' . '/' . $rename . $image_data['file_ext'];
                        break;
                    case 'medium':
                        $meta_image['medium'] = $base_path . 'medium' . '/' . $rename . $image_data['file_ext'];
                        break;
                    case 'large':
                        $meta_image['large'] = $base_path . 'large' . '/' . $rename . $image_data['file_ext'];
                        break;
                }

                $config_crop = array(
                    'image_library' => 'gd2',
                    'source_image' => $base_path . $value["type"] . '/' . $image_data['raw_name'] . $image_data['file_ext'],
                    'new_image' => $base_path . $value["type"] . '/' . $rename . $image_data['file_ext'],
                    'create_thumb' => false,
                    'maintain_ratio' => false,
                    'width' => $value['width'],
                    'height' => $value['height']
                );

                $this->image_lib->initialize($config_crop);
                if (!$this->image_lib->crop()) {
                    echo $this->image_lib->display_errors();
                }

                //DELETE RESIZE IMAGE
                unlink($base_path . $value["type"] . '/' . $image_data['raw_name'] . $image_data['file_ext']);
                $this->image_lib->clear();
            }

            rename($image_data['full_path'], $ori_path . $rename . $image_data['file_ext']);
            $format_upload = $rename . $image_data['file_ext'];
        }

        return $format_upload;
    }

}