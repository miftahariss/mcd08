<?php

class Frontend_model extends CI_Model
{
    public function result($idSoal,$idJawaban)
    {
        $query = $this->db->query("select *from kidnesia_mcd2018_kuis_ms where id='$idSoal' and sts_rc='1'");
        $queryRow = $query->row();
        $idjawab = $queryRow->id_jawaban;
        if($idjawab==$idJawaban)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    public function optionSoal($idSoal)
    {
        $query = $this->db->query("select *from kidnesia_mcd2018_kuis_option where id_soal='$idSoal'");

        return $query->result();
    }

    public function noSoal($id)
    {
        $query = $this->db->query("select *from kidnesia_mcd2018_kuis_member_jawaban where sts_rc='1' and idKuis='$id' and id_jawab != '' and id_jawab !=0");

        return $query->num_rows();
    }

    public function soal($id)
    {
        $query = $this->db->query("select *from kidnesia_mcd2018_kuis_ms where sts_rc='1' and id not in(select id_soal from kidnesia_mcd2018_kuis_member_jawaban where idKuis='$id' and id_jawab !='' and sts_rc='1') order by rand() limit 1");

        return $query->row();
    }

    public function getId($email)
    {
        $query = $this->db->query("select id from kidnesia_mcd2018_kuis_member where email='$email' and finish='0' order by date_in desc limit 1");
        return $query->row()->id;
    }

    public function savePhoto($data)
    {
        //INSERT
        $user['id'] = '';
        $user['idFromTwit'] = 'upload';
        $user['foto'] = $data['foto'];
        $user['pengirim'] = $data['nama'].'penyambung'.$data['email'];
        $user['teks'] = $data['alamat'].'penyambung'.$data['telepon'].'penyambung'.$data['caption'];
        $user['sts_rc'] = 1;
        $this->db->insert('kidnesia_mcd2018_twitPic', $user);
    }

    public function saveRegKuis($data)
    {
        //INSERT
        //$user['id'] = '';
        $user['nama'] = $data['nama'];
        $user['email'] = $data['email'];
        $user['alamat'] = $data['alamat'];
        $user['telepon'] = $data['telepon'];
        $user['date_in'] = date('Y-m-d H:i:s');
        $user['finish'] = 0;
        $user['sts_rc'] = 1;
        $this->db->insert('kidnesia_mcd2018_kuis_member', $user);
    }

    
    public function dataUserDetailFoto($id)
    {
        $query = $this->db->query("select *from kidnesia_mcd2018_twitPic where id='$id'");
        return $query->row();
    }
    
    public function dataUserDetail($id)
    {
        $query = $this->db->query("select *from kidnesia_mcd2018_peserta where sts_rc='1' and id='$id'");
        return $query->row();
    }

    public function dataArtikelDetail($id)
    {
        $query = $this->db->query("select *from kidnesia_mcd2018_articles where status='1' and id='$id'");
        return $query->row();
    }

    public function dataVideoDetail($id){
        $query = $this->db->query("select *from kidnesia_mcd2018_video where status='1' and id='$id'");
        return $query->row();
    }

    public function total_member_kuis($key)
    {
        $query = $this->db->query("select *from kidnesia_mcd2018_kuis_member where sts_rc='1' and (nama like '%".$key."%' or 
            email like '%".$key."%' or  
            telepon like '%".$key."%'
            )");
        return $query->num_rows();
    }

    public function total_member_puzzle($key)
    {
        $query = $this->db->query("select kidnesia_mcd2018_puzzle_member.*, s.total_waktu AS abc
            from kidnesia_mcd2018_puzzle_member 
            LEFT JOIN 
            (
                SELECT member_id, puzzle1_time, puzzle2_time, puzzle3_time, (puzzle1_time + puzzle2_time + puzzle3_time) AS total_waktu
                FROM kidnesia_mcd2018_puzzle_member_history
                WHERE 
                puzzle1_time != '00:00:00' AND 
                puzzle2_time != '00:00:00' AND 
                puzzle3_time != '00:00:00'
                ORDER BY total_waktu ASC
            ) 
            s ON s.member_id = kidnesia_mcd2018_puzzle_member.id
            WHERE 
            s.puzzle1_time != '00:00:00' AND 
            s.puzzle2_time != '00:00:00' AND 
            s.puzzle3_time != '00:00:00' AND
            kidnesia_mcd2018_puzzle_member.sts_rc='1' AND 
            (
            kidnesia_mcd2018_puzzle_member.nama like '%".$key."%' or 
            kidnesia_mcd2018_puzzle_member.email like '%".$key."%' or  
            kidnesia_mcd2018_puzzle_member.nomor_hp like '%".$key."%'
            ) 
            GROUP BY s.member_id
            ORDER BY s.total_waktu ASC");
        return $query->num_rows();
    }

    public function listMember_kuis_excel()
    {
        $query = $this->db->query("select *from kidnesia_mcd2018_kuis_member where sts_rc='1' order by nama asc");
        return $query->result();
    }

    public function listMember_puzzle_excel()
    {
        $query = $this->db->query("select *from kidnesia_mcd2018_puzzle_member where sts_rc='1' order by nama asc");
        return $query->result();
    }

    public function listMember_kuis($perPage,$limit,$key)
    {
        $query = $this->db->query("select *from kidnesia_mcd2018_kuis_member where sts_rc='1' and 
            (
            nama like '%".$key."%' or 
            email like '%".$key."%' or  
            telepon like '%".$key."%'
            ) 
            order by timing asc limit $limit,$perPage
            ");
        return $query->result();
    }

    public function listMember_puzzle($perPage,$limit,$key)
    {
        $query = $this->db->query("select kidnesia_mcd2018_puzzle_member.*, s.total_waktu AS abc
            from kidnesia_mcd2018_puzzle_member 
            LEFT JOIN 
            (
                SELECT member_id, puzzle1_time, puzzle2_time, puzzle3_time, (puzzle1_time + puzzle2_time + puzzle3_time) AS total_waktu
                FROM kidnesia_mcd2018_puzzle_member_history
                WHERE 
                puzzle1_time != '00:00:00' AND 
                puzzle2_time != '00:00:00' AND 
                puzzle3_time != '00:00:00'
                ORDER BY total_waktu ASC
            ) 
            s ON s.member_id = kidnesia_mcd2018_puzzle_member.id
            WHERE 
            s.puzzle1_time != '00:00:00' AND 
            s.puzzle2_time != '00:00:00' AND 
            s.puzzle3_time != '00:00:00' AND
            kidnesia_mcd2018_puzzle_member.sts_rc='1' AND 
            (
            kidnesia_mcd2018_puzzle_member.nama like '%".$key."%' or 
            kidnesia_mcd2018_puzzle_member.email like '%".$key."%' or  
            kidnesia_mcd2018_puzzle_member.nomor_hp like '%".$key."%'
            ) 
            GROUP BY s.member_id
            ORDER BY s.total_waktu ASC
            LIMIT $limit,$perPage
            ");
        return $query->result();
    }

    public function total_member($key)
    {
        $query = $this->db->query("select *from kidnesia_mcd2018_peserta where sts_rc='1' and (namaTeam like '%".$key."%' or 
            emailTeam like '%".$key."%' or 
            namaSekolah like '%".$key."%' or 
            namaPelatih like '%".$key."%' or 
            nomorTeleponPelatih like '%".$key."%' or 
            emailPelatih like '%".$key."%' or 
            alamatSekolah like '%".$key."%' or 
            namaGuru like '%".$key."%' or 
            nomorTeleponGuru like '%".$key."%' or 
            emailGuru like '%".$key."%'
            )");
        return $query->num_rows();
    }

    public function total_artikel($key)
    {
        $query = $this->db->query("select *from kidnesia_mcd2018_articles where status='1' and (title like '%".$key."%'
            )");
        return $query->num_rows();
    }

    public function total_member_foto($key)
    {
        $query = $this->db->query("select *from kidnesia_mcd2018_twitPic where (pengirim like '%".$key."%')");
        return $query->num_rows();
    }

    public function listMember_foto_excel()
    {
        $query = $this->db->query("select *from kidnesia_mcd2018_twitPic order by id desc");
        return $query->result();
    }

    public function listMember_foto($perPage,$limit,$key)
    {
        $query = $this->db->query("select *from kidnesia_mcd2018_twitPic where (
            pengirim like '%".$key."%'
            ) 
            order by id desc limit $limit,$perPage
            ");
        return $query->result();
    }
    
    public function listMemberExcel()
    {
        $query = $this->db->query("select *from kidnesia_mcd2018_peserta where sts_rc='1' order by namaTeam asc");
        return $query->result();
    }

    public function listArtikelExcel()
    {
        $query = $this->db->query("select *from kidnesia_mcd2018_articles where status='1' order by id desc");
        return $query->result();
    }

    public function listVideoExcel()
    {
        $query = $this->db->query("select *from kidnesia_mcd2018_video where status='1' order by id desc");
        return $query->result();
    }

    public function listMember($perPage,$limit,$key)
    {
        $query = $this->db->query("select *from kidnesia_mcd2018_peserta where sts_rc='1' and 
            (
            namaTeam like '%".$key."%' or 
            emailTeam like '%".$key."%' or 
            namaSekolah like '%".$key."%' or 
            namaPelatih like '%".$key."%' or 
            nomorTeleponPelatih like '%".$key."%' or 
            emailPelatih like '%".$key."%' or 
            alamatSekolah like '%".$key."%' or 
            namaGuru like '%".$key."%' or 
            nomorTeleponGuru like '%".$key."%' or 
            emailGuru like '%".$key."%'
            ) 
            order by namaTeam asc limit $limit,$perPage
            ");
        return $query->result();
    }

    public function listArtikel($perPage,$limit,$key)
    {
        $query = $this->db->query("select *from kidnesia_mcd2018_articles where status='1' and 
            (
            title like '%".$key."%'
            ) 
            order by id desc limit $limit,$perPage
            ");
        return $query->result();
    }

    public function listVideo(){
        $query = $this->db->query("select *from kidnesia_mcd2018_video where status='1' order by id desc");
        return $query->result();
    }

    public function total_twit_pic()
    {
        $query = $this->db->query("select *from kidnesia_mcd2018_twitPic where sts_rc='1'");
        return $query->num_rows();
    }

    public function listtwit_pic($perPage,$limit)
    {
        $query = $this->db->query("select *from kidnesia_mcd2018_twitPic where sts_rc='1' order by id desc limit $limit,$perPage");
        return $query->result();
    }

    public function total_team()
    {
        $query = $this->db->query("select *from kidnesia_mcd2018_peserta where sts_rc='1'");
        return $query->num_rows();
    }

    public function listTeam($perPage,$limit)
    {
        $query = $this->db->query("select *from kidnesia_mcd2018_peserta where sts_rc='1' order by namaTeam asc limit $limit,$perPage");
        return $query->result();
    }

    public function save($data)
    {
        $emailtimfutsal = $data['emailtimfutsal'];
        $cek = $this->db->query("select *from kidnesia_mcd2018_peserta where sts_rc='1' and emailTeam='$emailtimfutsal'")->num_rows;
        if($cek==0)
        {
            //INSERT
            $user['namaTeam'] = $data['namatimfutsal'];
            $user['fotoTeam'] = $data['namafileFotoTeam'];
            $user['emailTeam'] = $data['emailtimfutsal'];
            $user['password'] = $data['password'];
            $user['namaSekolah'] = $data['namasekolah'];
            $user['alamatSekolah'] = $data['alamatsekolah'];
            $user['namaPelatih'] = $data['namapelatih'];
            $user['fotoPelatih'] = $data['namafileFotoPelatih'];
            $user['tanggalLahirPelatih'] = $data['tanggal_lahir_pelatih'];
            $user['nomorTeleponPelatih'] = $data['nomorteleponpelatih'];
            $user['emailPelatih'] = $data['emailpelatih'];
            $user['namaGuru'] = $data['namaguruolahraga'];
            $user['fotoGuru'] = $data['namafileFotoGuru'];
            $user['tanggalLahirGuru'] = $data['tanggal_lahir_guru'];
            $user['nomorTeleponGuru'] = $data['nomorteleponguruolahraga'];
            $user['emailGuru'] = $data['emailguruolahraga'];
            $user['date_in'] = date('Y-m-d H:i:s');
            $user['sts_rc'] = 1;
            $this->db->insert('kidnesia_mcd2018_peserta', $user);

            $input_microsite['entry_date'] = date('Y-m-d H:i:s');
            $input_microsite['brand_name'] = 'Kidnesia';
            $input_microsite['microsite_name'] = 'McDonalds Junior Futsal Championship 2018';
            $input_microsite['facebook_id'] = '';
            $input_microsite['twitter_id'] = '';
            $input_microsite['name'] = $data['namatimfutsal'];
            $input_microsite['gender'] = '';
            $input_microsite['address'] = $data['alamatsekolah'];
            $input_microsite['city'] = '';
            $input_microsite['province'] = '';
            $input_microsite['phone'] = $data['nomorteleponpelatih'];
            $input_microsite['email'] = $data['emailtimfutsal'];
            $input_microsite['birth_date'] = $data['tanggal_lahir_pelatih'];
            $input_microsite['status'] = 1;
            $db2 = $this->load->database('second_db', TRUE);
            $db2->insert('microsite_entries', $input_microsite);
        }
        elseif($cek>0)
        {
            //UPDATE
            $user['namaTeam'] = $data['namatimfutsal'];
            $user['fotoTeam'] = $data['namafileFotoTeam'];
            $user['emailTeam'] = $data['emailtimfutsal'];
            $user['password'] = $data['password'];
            $user['namaSekolah'] = $data['namasekolah'];
            $user['alamatSekolah'] = $data['alamatsekolah'];
            $user['namaPelatih'] = $data['namapelatih'];
            $user['fotoPelatih'] = $data['namafileFotoPelatih'];
            $user['tanggalLahirPelatih'] = $data['tanggal_lahir_pelatih'];
            $user['nomorTeleponPelatih'] = $data['nomorteleponpelatih'];
            $user['emailPelatih'] = $data['emailpelatih'];
            $user['namaGuru'] = $data['namaguruolahraga'];
            $user['fotoGuru'] = $data['namafileFotoGuru'];
            $user['tanggalLahirGuru'] = $data['tanggal_lahir_guru'];
            $user['nomorTeleponGuru'] = $data['nomorteleponguruolahraga'];
            $user['emailGuru'] = $data['emailguruolahraga'];
            $user['date_up'] = date('Y-m-d H:i:s');
            $user['sts_rc'] = 1;

            $this->db->where('emailTeam', $user['emailTeam']);
            $this->db->update('kidnesia_mcd2018_peserta', $user);


            $input_microsite['entry_date'] = date('Y-m-d H:i:s');
            $input_microsite['brand_name'] = 'Kidnesia';
            $input_microsite['microsite_name'] = 'McDonalds Junior Futsal Championship 2018';
            $input_microsite['facebook_id'] = '';
            $input_microsite['twitter_id'] = '';
            $input_microsite['name'] = $data['namatimfutsal'];
            $input_microsite['gender'] = '';
            $input_microsite['address'] = $data['alamatsekolah'];
            $input_microsite['city'] = '';
            $input_microsite['province'] = '';
            $input_microsite['phone'] = $data['nomorteleponpelatih'];
            $input_microsite['email'] = $data['emailtimfutsal'];
            $input_microsite['birth_date'] = $data['tanggal_lahir_pelatih'];
            $input_microsite['status'] = 1;
            $db2 = $this->load->database('second_db', TRUE);
            $db2->where('email', $input_microsite['email']);
            $db2->update('microsite_entries', $input_microsite);
        }
    }

	public function angkaKeNamaBulan($str) 
    {
        if ($str == "1")
            {return "Januari";}
        else if ($str == "2")
            {return "Februari";}
        else if ($str == "3")
            {return "Maret";}
        else if ($str == "4")
            {return "April";}
        else if ($str == "5")
            {return "Mei";}
        else if ($str == "6")
            {return "Juni";}
        else if ($str == "7")
            {return "Juli";}
        else if ($str == "8")
            {return "Agustus";}
        else if ($str == "9")
            {return "September";}
        else if ($str == "10")
            {return "Oktober";}
        else if ($str == "11")
            {return "November";}
        else if ($str == "12")
            {return "Desember";}
        else 
            {return "-";}
    }

    public function ucapan()
    {
        $jamskrg = date('H:i:s');
        if($jamskrg>='00:00:00' && $jamskrg<='10:00:00')
        {
            $ucapan = "Selamat Pagi";
        }
        elseif($jamskrg>='10:00:01' && $jamskrg<='14:00:00')
        {
            $ucapan = "Selamat Siang";
        }
        elseif($jamskrg>='14:00:01' && $jamskrg<='18:00:00')
        {
            $ucapan = "Selamat Sore";
        }
        else
        {
            $ucapan = "Selamat Malam";
        }
        return $ucapan;
    }

    public function lihatHari($tanggal)
    {
        $tahun = substr($tanggal,0,4);
        $bulan = substr($tanggal,5,2);
        $tanggal = substr($tanggal,8,2);
        $bulanTeks = $this->angkaKeNamaBulan($bulan);
        $lihatHari= $tanggal." ".$bulanTeks." ".$tahun;
        return $lihatHari;
    }

    public function hariIndonesia($hari)
    {
        if($hari=="Monday")
        {
            $hari = "Senin";
        }
        elseif($hari=="Tuesday")
        {
            $hari = "Selasa";
        }
        elseif($hari=="Wednesday")
        {
            $hari = "Rabu";
        }
        elseif($hari=="Thursday")
        {
            $hari = "Kamis";
        }
        elseif($hari=="Friday")
        {
            $hari = "Jumat";
        }
        elseif($hari=="Saturday")
        {
            $hari = "Sabtu";
        }
        elseif($hari=="Sunday")
        {
            $hari = "Minggu";
        }
        return $hari;
    }
    
	
    
}
