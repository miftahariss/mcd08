
<div class="row" style="width:95%; height:2000px;">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                TAMBAH ARTIKEL
                
				<div align="right" style="margin-top:-25px;">
				<a href="<?= site_url('article/dataartikel/all')?>" class="btn btn-primary btn-sm">Kembali</a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <form method="POST" enctype="multipart/form-data">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <tbody>
                            <tr>
                                <th width="20%">Title <span style="color:red;"><?php echo form_error('title')?></span></th>
                                <td><input type="text" name="title" style="width:100%" required=""></td>
                            </tr>
                            <tr>
                                <th width="20%">Body <span style="color:red;"><?php echo form_error('body')?></span></th>
                                <td><textarea id="editor1" name="body" required></textarea></td>
                            </tr>
                            <tr>
                                <th width="20%">Thumbnail <span style="color:red;"><?php echo form_error('thumbnail')?></span></th>
                                <td>
                                    <!-- <img src="<?=base_url()?>assets/frontend/images/foto_artikel/<?=$user->thumbnail?>" width="400px"> -->
                                    <input type="file" name="userfile" required>
                                </td>
                            </tr>
                            <tr>
                                <th width="20%"></th>
                                <td><input type="submit" name="submit"></td>
                            </tr>
                        </tbody>
                    </table>
                    </form>
                </div>
               
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/backend/plugins/ckeditor/ckeditor.js"></script>
<script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace( 'editor1' );
</script>

