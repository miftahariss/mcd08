
<div class="row" style="width:95%; height:2000px;">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                TAMBAH VIDEO
                
				<div align="right" style="margin-top:-25px;">
				<a href="<?= site_url('video/datavideo/all')?>" class="btn btn-primary btn-sm">Kembali</a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <form method="POST" enctype="multipart/form-data">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <tbody>
                            <tr>
                                <th width="20%">Video URL <span style="color:red;"><?php echo form_error('video_url')?></span></th>
                                <td><input type="text" name="video_url" style="width:100%" required=""></td>
                            </tr>
                            <tr>
                                <th width="20%">Caption <span style="color:red;"><?php echo form_error('caption')?></span></th>
                                <td><input type="text" name="caption" style="width:100%" required=""></td>
                            </tr>
                            <tr>
                                <th width="20%">Video Home</th>
                                <td><input type="checkbox" name="is_home" value="1"></td>
                            </tr>
                            <tr>
                                <th width="20%">Thumbnail <span style="color:red;"><?php echo form_error('thumbnail')?></span></th>
                                <td>
                                    <input type="file" name="userfile" required>
                                </td>
                            </tr>
                            <tr>
                                <th width="20%"></th>
                                <td><input type="submit" name="submit"></td>
                            </tr>
                        </tbody>
                    </table>
                    </form>
                </div>
               
            </div>
        </div>
    </div>
</div>

