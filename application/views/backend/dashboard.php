
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/backend/js/highcharts.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/css/bootstrap-datepicker.css">

<script>
function submitSearch()
{
    var dari = document.getElementById("dari").value;

    var find = '/';
    var re = new RegExp(find, 'g');
    dariUrl = dari.replace(re, '-');

    var sampai = document.getElementById("sampai").value;
    var find = '/';
    var re = new RegExp(find, 'g');
    sampaiUrl = sampai.replace(re, '-');

    window.location.href = "<?php echo base_url().'main/dashboard/'?>"+dariUrl+"/"+sampaiUrl;
}
</script>
<?php
$tahunDari = substr($dari,6,4);
$bulanDari = substr($dari,3,2);
$tanggalDari = substr($dari,0,2);
$dariGabung = $tahunDari.'-'.$bulanDari.'-'.$tanggalDari.' 00:00:00'; 

$tahunSampai = substr($sampai,6,4);
$bulanSampai = substr($sampai,3,2);
$tanggalSampai = substr($sampai,0,2);
$sampaiGabung = $tahunSampai.'-'.$bulanSampai.'-'.$tanggalSampai.' 23:59:59';
?>
<script type="text/javascript">
	var chart2; // globally available
    var chart3;
    var chart4;

	$(document).ready(function() {

        <?php /* ?>
		chart2 = new Highcharts.Chart({
         chart: {
            renderTo: 'containerTeam',
            type: 'column',
			width:'475'
         },   
         title: {
            text: 'Team Peserta'
         },
         xAxis: {
            categories: ['Peserta']
         },
         yAxis: {
            title: {
               text: 'Peserta'
            }
         },
              series:             
            [
            	{
            		<?php
            		$que = "select *from kidnesia_mcd2018_peserta where sts_rc='1' and date_in >= '$dariGabung' and date_in<='$sampaiGabung'";
            		$jml = $this->db->query($que)->num_rows();
            		?>
	            	name: 'Peserta',
	                
	                data: [<?=$jml?>]
	            },
            ]
        });
        <?php */ ?>

        chart3 = new Highcharts.Chart({
         chart: {
            renderTo: 'containerKuis',
            type: 'column',
            width:'475'
            /*events: {
                click: function(event) {
                    var grab = event.explicitOriginalTarget.data;
                    var idUser = grab.split("-");
                    var idUserAsli = idUser[1];
                    alert('okk');
                    window.location = "<?=base_url().'masakdapur/recipe_list_user/'.$dariGabung.'/'.$sampaiGabung?>";
                }         
            }*/
         },   
         title: {
            text: 'Peserta Kuis'
         },
         xAxis: {
            categories: ['Peserta Kuis']
         },
         yAxis: {
            title: {
               text: 'Peserta Kuis'
            }
         },
              series:             
            [
                {
                    <?php
                    $que = "select *from kidnesia_mcd2018_kuis_member where sts_rc='1'";
                    $jml = $this->db->query($que)->num_rows();
                    ?>
                    name: 'Peserta Kuis',
                    
                    data: [<?=$jml?>]
                },
            ]
        });

        chart4 = new Highcharts.Chart({
         chart: {
            renderTo: 'containerPuzzle',
            type: 'column',
            width:'475'
            /*events: {
                click: function(event) {
                    var grab = event.explicitOriginalTarget.data;
                    var idUser = grab.split("-");
                    var idUserAsli = idUser[1];
                    alert('okk');
                    window.location = "<?=base_url().'masakdapur/recipe_list_user/'.$dariGabung.'/'.$sampaiGabung?>";
                }         
            }*/
         },   
         title: {
            text: 'Peserta Puzzle'
         },
         xAxis: {
            categories: ['Peserta Puzzle']
         },
         yAxis: {
            title: {
               text: 'Peserta Puzzle'
            }
         },
              series:             
            [
                {
                    <?php
                    $que = "select *from kidnesia_mcd2018_puzzle_member where sts_rc='1'";
                    $jml = $this->db->query($que)->num_rows();
                    ?>
                    name: 'Peserta Puzzle',
                    
                    data: [<?=$jml?>]
                },
            ]
        });
   });		
</script>


<div class="row" style="width:1050px; overflow-x:scroll; ">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Dashboard
                <div align="right" style="margin-top:-25px;">
                    <?php
                    $date = date('d-m-Y');
                    ?>
                    <a href="<?= site_url('main/dashboard/01-01-1970/'.$date)?>" class="btn btn-primary btn-sm">Refresh</a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
					<table style="width:50%;" class="table table-striped table-bordered table-hover" id="dataTables-example">
					    <thead>
					    <tr>
					    <th width="20%">Dari</th>
					    <th width="20%">Sampai</th>
					    </tr>
					    </thead>
					    <tbody>
					    <tr>
					    <td>
					    <input type="text" readonly="readonly" style="width:200px;" class="datepicker form-control" name="dari" id="dari" value="<?php echo $dari?>" />
					    </td>
					    <td><input readonly="readonly" type="text" style="width:200px;" class="datepicker form-control" name="sampai" id="sampai" value="<?php echo $sampai?>" /></td>
					    </tr>
					    <tr>
					    <td colspan="5"><div align="right"><input type="button" class="btn btn-primary btn-sm" onclick="submitSearch()" id="submitbutton" value="Submit" /></div></td>
					    </tr>
					    </tbody>
					</table>



					<table style="width:100%;" class="table table-striped table-bordered table-hover" id="dataTables-example">
					<tr>
						<!-- <td width="50%">
						  <div style="width:100%; margin-right:5px; margin-bottom:10px; float:left;" id='containerTeam'></div>
						</td> -->
                        <td width="50%">
                          <div style="width:100%; margin-right:5px; margin-bottom:10px; float:left;" id='containerKuis'></div>
                        </td>
                        <td width="50%">
                          <div style="width:100%; margin-right:5px; margin-bottom:10px; float:left;" id='containerPuzzle'></div>
                        </td>
					</tr>
                    
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
   $('.datepicker').datepicker({
       weekStart:1,
       color: 'red'
   });
</script>