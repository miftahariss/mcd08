
<div class="row" style="width:95%; height:2000px;">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                DETAIL ARTIKEL
                
				<div align="right" style="margin-top:-25px;">
				<a href="<?= site_url('article/dataartikel/all')?>" class="btn btn-primary btn-sm">Kembali</a>
                <a href="<?= site_url('article/detailartikel/'.$user->id)?>" class="btn btn-primary btn-sm">Refresh</a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <tbody>
                            <tr>
                                <th width="20%">Title</th>
                                <td><?=$user->title?></td>
                            </tr>
                            <tr>
                                <th width="20%">Body</th>
                                <td><?=$user->body?></td>
                            </tr>
                            <tr>
                                <th width="20%">Thumbnail</th>
                                <td>
                                    <img src="<?=base_url()?>uploads/images/original/<?=$user->thumbnail?>" width="400px">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
               
            </div>
        </div>
    </div>
</div>