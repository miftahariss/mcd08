
<div class="row" style="width:95%; height:2000px;">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                DETAIL TEAM
                
				<div align="right" style="margin-top:-25px;">
				<a href="<?= site_url('member/datamember/all')?>" class="btn btn-primary btn-sm">Kembali</a>
                <a href="<?= site_url('member/detailuser/'.$user->id)?>" class="btn btn-primary btn-sm">Refresh</a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <tbody>
                            <tr>
                                <th colspan="2"><div align="center">IDENTITAS TEAM</div></th>
                            </tr>
                            <tr>
                                <th width="20%">Nama Team</th>
                                <td><?=$user->namaTeam?></td>
                            </tr>
                            <tr>
                                <th width="20%">Tanggal Registrasi</th>
                                <td><?=date('d-M-Y',strtotime($user->date_in))?></td>
                            </tr>
                            <tr>
                                <th width="20%">Foto Team</th>
                                <td>
                                    <img src="<?=base_url()?>assets/frontend/images/foto_team/<?=$user->fotoTeam?>" width="400px">
                                </td>
                            </tr>
                            <tr>
                                <th width="20%">Email Team</th>
                                <td><?=$user->emailTeam?></td>
                            </tr>
                            <tr>
                                <th width="20%">Nama Sekolah</th>
                                <td><?=$user->namaSekolah?></td>
                            </tr>
                            <tr>
                                <th width="20%">Alamat Sekolah</th>
                                <td><?=$user->alamatSekolah?></td>
                            </tr>
                            <tr>
                                <th width="20%">Nama Pelatih</th>
                                <td><?=$user->namaPelatih?></td>
                            </tr>
                            <tr>
                                <th width="20%">Foto Pelatih</th>
                                <td><img src="<?=base_url()?>assets/frontend/images/foto_pelatih/<?=$user->fotoPelatih?>" width="400px"></td>
                            </tr>
                            <tr>
                                <th width="20%">Tanggal Lahir Pelatih</th>
                                <td><?=date('d-M-Y',strtotime($user->tanggalLahirPelatih))?></td>
                            </tr>
                            <tr>
                                <th width="20%">Nomor Telepon Pelatih</th>
                                <td><?=$user->nomorTeleponPelatih?></td>
                            </tr>
                            <tr>
                                <th width="20%">Email Pelatih</th>
                                <td><?=$user->emailPelatih?></td>
                            </tr>
                            <tr>
                                <th width="20%">Nama Guru</th>
                                <td><?=$user->namaGuru?></td>
                            </tr>
                            <tr>
                                <th width="20%">Foto Guru</th>
                                <td><img src="<?=base_url()?>assets/frontend/images/foto_guru/<?=$user->fotoGuru?>" width="400px"></td>
                            </tr>
                            <tr>
                                <th width="20%">Tanggal Lahir Guru</th>
                                <td><?=date('d-M-Y',strtotime($user->tanggalLahirGuru))?></td>
                            </tr>
                            <tr>
                                <th width="20%">Nomor Telepon Guru</th>
                                <td><?=$user->nomorTeleponGuru?></td>
                            </tr>
                            <tr>
                                <th width="20%">Email Guru</th>
                                <td><?=$user->emailGuru?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
               
            </div>
        </div>
    </div>
</div>