
<div class="row" style="width:95%; height:2000px;">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                DETAIL USER PHOTO CONTEST
                
				<div align="right" style="margin-top:-25px;">
				<a href="<?= site_url('member/datamemberfoto/all')?>" class="btn btn-primary btn-sm">Kembali</a>
                <a href="<?= site_url('member/detailuserfoto/'.$user->id)?>" class="btn btn-primary btn-sm">Refresh</a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <?php
                    if($user->idFromTwit=="upload")
                    {
                        $foto = base_url().'assets/frontend/images/twitpic/'.$user->foto;

                        $namaPecah = explode("penyambung",$user->pengirim);
                        if(count($namaPecah)>0)
                        {
                            if(isset($namaPecah[0]))
                            {
                                $nama = $namaPecah[0];
                            }
                            else
                            {
                                $nama = '';
                            }
                            if(isset($namaPecah[1]))
                            {
                                $email = $namaPecah[1];
                            }
                            else
                            {
                                $email = '';
                            }
                            
                        }
                        else
                        {
                            $nama = '';
                            $email = '';
                        }

                        $teksPecah = explode("penyambung",$user->teks);
                        if(count($teksPecah)>0)
                        {
                            if(isset($teksPecah[0]))
                            {
                                $alamat = $teksPecah[0];
                            }
                            else
                            {
                                $alamat = '';
                            }

                            if(isset($teksPecah[1]))
                            {
                                $telepon = $teksPecah[1];
                            }
                            else
                            {
                                $telepon = '';
                            }

                            if(isset($teksPecah[2]))
                            {
                                $caption = $teksPecah[2];
                            }
                            else
                            {
                                $caption = '';
                            }
                        }
                        else
                        {
                            $alamat = '';
                            $telepon = '';
                            $caption = '';
                        }
                        
                    }
                    else
                    {
                        $nama = '@'.$user->pengirim;
                        $email = '-';
                        $alamat = '';
                        $telepon = '';
                        $caption = $user->teks;
                        $foto = $user->foto;
                    }
                    ?>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <tbody>
                            <tr>
                                <th colspan="2"><div align="center">IDENTITAS USER PHOTO CONTEST</div></th>
                            </tr>
                            <tr>
                                <th width="20%">Nama</th>
                                <td><?=$nama?></td>
                            </tr>
                            <tr>
                                <th width="20%">Email</th>
                                <td><?=$email?></td>
                            </tr>
                            <tr>
                                <th width="20%">Alamat</th>
                                <td>
                                    <?=$alamat?>
                                </td>
                            </tr>
                            <tr>
                                <th width="20%">Nomor Telepon</th>
                                <td><?=$telepon?></td>
                            </tr>
                            <tr>
                                <th width="20%">Caption</th>
                                <td><?=$caption?></td>
                            </tr>
                            <tr>
                                <th width="20%">Foto</th>
                                <td><img src="<?=$foto?>" width="200px"></td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
               
            </div>
        </div>
    </div>
</div>