<?php
if($act=='futsal')
{
    $namaFIle = 'futsal';
}
elseif($act=='foto')
{
    $namaFIle = 'Photo_Contest';
}
elseif($act=='kuis')
{
    $namaFIle = 'Kuis';
}
elseif ($act=='puzzle')
{
    $namaFIle = 'Puzzle';
}
elseif ($act == 'artikel') {
    $namaFIle = 'Artikel';
}
elseif($act == 'video'){
    $namaFIle = 'Video';
}

header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=".$namaFIle.".xls");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
?>

<table border=1>
    <tr>
        <?php
        if($act=='futsal')
        {
        ?>
            <th><small>No</small></th>
            <th><small>Nama Team</small></th>
            <th><small>Tanggal Registrasi</small></th>
            <th><small>Foto Team</small></th>
            <th><small>Email Team</small></th>
            <th><small>Nama Sekolah</small></th>
            <th><small>Alamat Sekolah</small></th>
            <th><small>Nama Pelatih</small></th>
            <th><small>Foto Pelatih</small></th>
            <th><small>Tanggal Lahir Pelatih</small></th>
            <th><small>Nomor Telepon Pelatih</small></th>
            <th><small>Email Pelatih</small></th>
            <th><small>Nama Guru</small></th>
            <th><small>Foto Guru</small></th>
            <th><small>Tanggal Lahir Guru</small></th>
            <th><small>Nomor Telepon Guru</small></th>
            <th><small>Email Guru</small></th>
        <?php
        }
        elseif($act=='kuis')
        {
        ?>
            <th><small>No</small></th>
            <th><small>Nama</small></th>
            <th><small>Email</small></th>
            <th><small>Alamat</small></th>
            <th><small>No Telepon</small></th>
            <th><small>Jawab Benar</small></th>
            <th><small>Jawab Salah</small></th>
            <th><small>Total Waktu</small></th>
        <?php
        }
        elseif($act=='foto')
        {
        ?>
            <th><small>No</small></th>
            <th><small>Nama</small></th>
            <th><small>Email</small></th>
            <th><small>Alamat</small></th>
            <th><small>Nomor Telepon</small></th>
            <th><small>Caption</small></th>
            <th><small>Foto</small></th>
        <?php
        }
        elseif($act=='puzzle')
        {
        ?>
            <th><small>No</small></th>
            <th><small>Nama</small></th>
            <th><small>Email</small></th>
            <th><small>Alamat</small></th>
            <th><small>No Telepon</small></th>
            <th><small>Total Waktu</small></th>
        <?php
        }
        elseif($act=='artikel')
        {
        ?>
            <th><small>No</small></th>
            <th><small>Title</small></th>
            <th><small>Created At</small></th>
        <?php
        }
        ?>
    </tr>

        <?php
        if($act=='futsal')
        {
            $no=1;
            foreach($list_member as $data)
            {
            ?>
                <tr>
                <th><small><?=$no?></small></th>
                <th><small><?=$data->namaTeam?></small></th>
                <th><small><?=date('d-M-Y',strtotime($data->date_in))?></small></th>
                <th><small><a href="<?=base_url()?>assets/frontend/images/foto_team/<?=$data->fotoTeam?>"><?=base_url()?>assets/frontend/images/foto_team/<?=$data->fotoTeam?></a></small></th>
                <th><small><?=$data->emailTeam?></small></th>
                <th><small><?=$data->namaSekolah?></small></th>
                <th><small><?=$data->alamatSekolah?></small></th>
                <th><small><?=$data->namaPelatih?></small></th>
                <th><small><a href="<?=base_url()?>assets/frontend/images/foto_pelatih/<?=$data->fotoPelatih?>"><?=base_url()?>assets/frontend/images/foto_pelatih/<?=$data->fotoPelatih?></a></small></th>
                <th><small><?=date('d-M-Y',strtotime($data->tanggalLahirPelatih))?></small></th>
                <th><small><?=$data->nomorTeleponPelatih?></small></th>
                <th><small><?=$data->emailPelatih?></small></th>
                <th><small><?=$data->namaGuru?></small></th>
                <th><small><a href="<?=base_url()?>assets/frontend/images/foto_guru/<?=$data->fotoGuru?>"><?=base_url()?>assets/frontend/images/foto_guru/<?=$data->fotoGuru?></a></small></th>
                <th><small><?=date('d-M-Y',strtotime($data->tanggalLahirGuru))?></small></th>
                <th><small><?=$data->nomorTeleponGuru?></small></th>
                <th><small><?=$data->emailGuru?></small></th>
                </tr>
            <?php
            $no++;
            }
            
            ?>
        <?php
        }
        elseif($act=='kuis')
        {
            $no=1;
            foreach($list_member as $data)
            {
            ?>
                <tr>
                <th><small><?=$no?></small></th>
                <th><small><?=$data->nama?></small></th>
                <th><small><?=$data->email?></small></th>
                <th><small><?=$data->alamat?></small></th>
                <th><small><?=$data->telepon?></small></th>
                <th><small>
                        <?php
                        if($data->benar != 0 && $data->benar != '')
                        {
                            echo $data->benar;
                        }
                        else
                        {
                            echo $queryBenar = $this->db->query("select id from kidnesia_mcd2018_kuis_member_jawaban where idKuis='$data->id' and sts_rc='1' and result='1'")->num_rows();
                        }
                        
                        ?>
                </small></th>
                <th><small>
                    <?php
                    if($data->salah != 0 && $data->salah != '')
                    {
                        echo $data->salah;
                    }
                    else
                    {
                        echo $queryBenar = $this->db->query("select id from kidnesia_mcd2018_kuis_member_jawaban where idKuis='$data->id' and sts_rc='1' and result='0'")->num_rows();
                    }
                    
                    ?>
                </small></th>
                <th><small><?=$data->timing?></small></th>
                </tr>
            <?php
            $no++;
            }
            
            ?>
        <?php
        }
        elseif($act=='puzzle')
        {
            $no=1;
            foreach($list_member as $data)
            {
            ?>
                <tr>
                <th><small><?=$no?></small></th>
                <th><small><?=$data->nama?></small></th>
                <th><small><?=$data->email?></small></th>
                <th><small><?=$data->alamat?></small></th>
                <th><small><?=$data->telepon?></small></th>
                <th><small><?=$data->timing?></small></th>
                </tr>
            <?php
            $no++;
            }
            ?>

        <?php
        }
        elseif($act=='foto')
        {
            $no=1;
            foreach($list_member as $data)
            {
                if($data->idFromTwit=="upload")
                {
                    $foto = base_url().'assets/frontend/images/twitpic/'.$data->foto;

                    $namaPecah = explode("penyambung",$data->pengirim);
                    if(count($namaPecah)>0)
                    {
                        if(isset($namaPecah[0]))
                        {
                            $nama = $namaPecah[0];
                        }
                        else
                        {
                            $nama = '';
                        }
                        if(isset($namaPecah[1]))
                        {
                            $email = $namaPecah[1];
                        }
                        else
                        {
                            $email = '';
                        }
                        
                    }
                    else
                    {
                        $nama = '';
                        $email = '';
                    }

                    $teksPecah = explode("penyambung",$data->teks);
                    if(count($teksPecah)>0)
                    {
                        if(isset($teksPecah[0]))
                        {
                            $alamat = $teksPecah[0];
                        }
                        else
                        {
                            $alamat = '';
                        }

                        if(isset($teksPecah[1]))
                        {
                            $telepon = $teksPecah[1];
                        }
                        else
                        {
                            $telepon = '';
                        }

                        if(isset($teksPecah[2]))
                        {
                            $caption = $teksPecah[2];
                        }
                        else
                        {
                            $caption = '';
                        }
                    }
                    else
                    {
                        $alamat = '';
                        $telepon = '';
                        $caption = '';
                    }
                    
                }
                else
                {
                    $nama = '@'.$data->pengirim;
                    $email = '-';
                    $alamat = '';
                    $telepon = '';
                    $caption = $data->teks;
                    $foto = $data->foto;
                }
                ?>
                <tr>
                <th><small>No</small></th>
                <th><small><?=$nama?></small></th>
                <th><small><?=$email?></small></th>
                <th><small><?=$alamat?></small></th>
                <th><small><?=$telepon?></small></th>
                <th><small><?=$caption?></small></th>
                <th><small><a href="<?=$foto?>"><?=$foto?></small><a/></small></th>
                </tr>
            <?php
            $no++;
            }
            ?>
        <?php
        } elseif($act == 'artikel'){

            $no=1;
            foreach($list_member as $data)
            {
            ?>
                <tr>
                <th><small><?=$no?></small></th>
                <th><small><?=$data->title?></small></th>
                <th><small><?=$data->created_at?></small></th>
                </tr>
            <?php
            $no++;
            }
            
            ?>

        <?php
        } elseif($act == 'video'){
            $no=1;
            foreach($list_member as $data)
            {
            ?>
                <tr>
                <th><small><?=$no?></small></th>
                <th><small><?=$data->video_url?></small></th>
                <th><small><?=$data->caption?></small></th>
                <th><a href="<?=base_url().'uploads/images/original/'.$data->thumbnail?>" target="_blank"><small><?=base_url().'uploads/images/original/'.$data->thumbnail?></small></a></th>
                <th><small><?=$data->created_at?></small></th>
                </tr>
            <?php
            $no++;
            }
            
            ?>

        <?php
        }
        ?>
    
        

</table>