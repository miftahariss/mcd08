<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <script language='JavaScript'>
  var txt="Admin : Mcd Futsal Championship 2018 - ";
  var kecepatan=100;var segarkan=null;function bergerak() {

  document.title=txt;
  txt=txt.substring(1,txt.length)+txt.charAt(0);
  segarkan=setTimeout("bergerak()",kecepatan);

  }bergerak();
  </script> 
  <link  href="<?=base_url()?>assets/frontend/images/logo-saji.png" rel="shortcut icon" />

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/css/style_login.css">

     <meta charset="UTF-8" />
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
</head>
    <!-- END HEAD -->

    <!-- BEGIN BODY -->
<body style="background-color:#E7E8EF;">

   <!-- PAGE CONTENT --> 
    <section class="container">
    <div class="login">
    <?php
    if($this->session->flashdata('message_success'))
    {
    ?>
    <div class="alert alert-success" style="width:90%;">
        
        <strong>Info!</strong> <?=$this->session->flashdata('message_success')?>
    </div>
    <br>
    <?php
    }
    ?>

    <?php
    if($this->session->flashdata('message_warning'))
    {
    ?>
        <div class="alert alert-warning" style="width:90%;">
            
            <strong>Info!</strong> <?=$this->session->flashdata('message_warning')?>
        </div>
        <br>
    <?php
    }
    ?>
      <h1><img style="margin-top:20px;" src="<?=base_url().'assets/frontend/images/logo.png'?>" width="150px"></h1>
      <form method="post" action="<?= site_url('login')?>" id="form1">
        <p><input <?php if(isset($_COOKIE['usernamemcd2018'])){?> value="<?=$_COOKIE['usernamemcd2018']?>" <?php } ?> type="text" name="username" value="" placeholder="Username"></p>
        <p><input <?php if(isset($_COOKIE['passwordmcd2018'])){?> value="<?=$_COOKIE['passwordmcd2018']?>" <?php } ?> type="password" name="password" value="" placeholder="Password"></p>
        <p class="remember_me" style="margin-top:-5px;">
          <label>
            <input <?php if(isset($_COOKIE['usernamemcd2018']) && isset($_COOKIE['passwordmcd2018'])){?> checked="checked" <?php } ?> type="checkbox" name="remember_me" id="remember_me">
            Remember me on this computer
          </label>
        </p>
        <br>
        <p>
          <button style="width:100%;" type="submit" class="btn btn-primary">Submit</button>
        </p>
      </form>
    </div>
  </section>
</body>
</html>
