<?php
if($key=="all")
{
    $key = "";
}
?>

<div style="float:left; margin-top:10px;">
<input type="text" name="key" id="key" value="<?=$key?>" class="form-control" style="width:100%;" placeholder="Email">
</div>
<div style="float:left; margin-top:12px; margin-left:10px;">
<input type="button" onclick="pencarian()" value="Search" class="btn btn-primary btn-sm" name="submit">
</div>
<br><br><br>
<div class="row" style="width:95%;">
    <div class="col-lg-12">
        <?php
        if($this->session->flashdata('message_success'))
        {
        ?>
        <div class="alert alert-success" style="width:60%;">
            
            <strong>Info!</strong> <?=$this->session->flashdata('message_success')?>
        </div>
        <?php
        }
        ?>
        
        <?php
        if($this->session->flashdata('message_warning'))
        {
        ?>
            <div class="alert alert-warning" style="width:60%;">
                
                <strong>Info!</strong> <?=$this->session->flashdata('message_warning')?>
            </div>
        <?php
        }
        ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                DAFTAR PESERTA
                <div align="right" style="margin-top:-25px;">
                <a href="<?=base_url()?>member/downloadExcel/futsal" class="btn btn-success btn-sm"><img src="<?php echo base_url(); ?>assets/backend/img/excel.png" style="margin-right:10px;" height="18px" width="30px">Download Excel</a>
                <a href="<?= site_url('member/datamember/all')?>" class="btn btn-primary btn-sm">Refresh</a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Team</th>
                                <th>Email Team</th>
                                <th>Nama Sekolah</th>
                                <th>Pelatih</th>
                                <th>Guru Penanggung Jawab</th>
                                <th></th>
                            <tbody>
                                <?php
                                $no=1;
                                if(count($list_member) > 0)
                                {
                                foreach($list_member as $list_member_d)
                                {
                                ?>
                                <tr class="odd gradeX">
                                    <td><?=$no+$currentPage?></td>
                                    <td><?=$list_member_d->namaTeam?></td>
                                    <td><?=$list_member_d->emailTeam?></td>
                                    <td class="center"><?=$list_member_d->namaSekolah?></td>
                                    <td class="center"><?=$list_member_d->namaPelatih?></td>
                                    <td class="center"><?=$list_member_d->namaGuru?></td>
                                    
                                    
                                    <td class="center">
                                    <div align="right">
                                        <a href="<?=site_url('member/detailuser/'.$list_member_d->id)?>" class="btn btn-success btn-sm">Detail</a>
                                        <a href="#" onclick="confirmation('<?=$list_member_d->id?>')" class="btn btn-danger btn-sm">X</a>
                                    </div>
                                    </td>
                                </tr>
                                <?php
                                $no++;
                                }
                                }
                                else
                                {
                                ?>
                                    <tr class="odd gradeX">
                                    <td colspan="7"><div align="center">-Tidak Ada Data-</div></td>
                                    </td>
                                </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                                
                            </tr>
                        </thead>
                    </table>
                    <div align="center">
                    <div class="pagination">
                    <ul>
                    <?=$this->pagination->create_links()?>
                    </ul>
                    </div>
                    </div>
                    <br><br><br><br><br><br><br><br><br><br>
                    <br><br><br><br><br><br><br><br><br><br>
                    <br><br><br><br><br><br><br><br><br><br>
                    <br><br><br><br><br><br><br><br><br><br>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

function confirmation(id,lomba)
{
    var jawab;
    jawab = confirm("Apakah Anda Yakin ?");
    if(jawab)
    {
        window.location = "<?=site_url('member/hapus/')?>/"+id;
    }
}

function pencarian()
{
    var key = document.getElementById("key").value;

    if(key=="" || key==null)
    {
        key = "all";
    }

    window.location = "<?=base_url().'member/datamember/'?>"+key; 
}
</script>