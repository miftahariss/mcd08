<?php
if($key=="all")
{
    $key = "";
}
?>

<div style="float:left; margin-top:10px;">
<input type="text" name="key" id="key" value="<?=$key?>" class="form-control" style="width:100%;" placeholder="Email">
</div>
<div style="float:left; margin-top:12px; margin-left:10px;">
<input type="button" onclick="pencarian()" value="Search" class="btn btn-primary btn-sm" name="submit">
</div>
<br><br><br>
<div class="row" style="width:95%;">
    <div class="col-lg-12">
        <?php
        if($this->session->flashdata('message_success'))
        {
        ?>
        <div class="alert alert-success" style="width:60%;">
            
            <strong>Info!</strong> <?=$this->session->flashdata('message_success')?>
        </div>
        <?php
        }
        ?>
        
        <?php
        if($this->session->flashdata('message_warning'))
        {
        ?>
            <div class="alert alert-warning" style="width:60%;">
                
                <strong>Info!</strong> <?=$this->session->flashdata('message_warning')?>
            </div>
        <?php
        }
        ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                DAFTAR PESERTA FOTO CONTEST
                <div align="right" style="margin-top:-25px;">
                <a href="<?=base_url()?>member/downloadExcel/foto" class="btn btn-success btn-sm"><img src="<?php echo base_url(); ?>assets/backend/img/excel.png" style="margin-right:10px;" height="18px" width="30px">Download Excel</a>
                <a href="<?= site_url('member/datamemberfoto/all')?>" class="btn btn-primary btn-sm">Refresh</a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Caption</th>
                                <th>Foto</th>
                                <th>Tampilkan</th>
                                <th></th>
                            <tbody>
                                <?php
                                $no=1;
                                if(count($list_member) > 0)
                                {
                                foreach($list_member as $list_member_d)
                                {
                                    if($list_member_d->idFromTwit=="upload")
                                    {
                                        $foto = base_url().'assets/frontend/images/twitpic/'.$list_member_d->foto;

                                        $namaPecah = explode("penyambung",$list_member_d->pengirim);
                                        if(count($namaPecah)>0)
                                        {
                                            if(isset($namaPecah[0]))
                                            {
                                                $nama = $namaPecah[0];
                                            }
                                            else
                                            {
                                                $nama = '';
                                            }
                                            if(isset($namaPecah[1]))
                                            {
                                                $email = $namaPecah[1];
                                            }
                                            else
                                            {
                                                $email = '';
                                            }
                                            
                                        }
                                        else
                                        {
                                            $nama = '';
                                            $email = '';
                                        }

                                        $teksPecah = explode("penyambung",$list_member_d->teks);
                                        if(count($teksPecah)>0)
                                        {
                                            if(isset($teksPecah[0]))
                                            {
                                                $alamat = $teksPecah[0];
                                            }
                                            else
                                            {
                                                $alamat = '';
                                            }

                                            if(isset($teksPecah[1]))
                                            {
                                                $telepon = $teksPecah[1];
                                            }
                                            else
                                            {
                                                $telepon = '';
                                            }

                                            if(isset($teksPecah[2]))
                                            {
                                                $caption = $teksPecah[2];
                                            }
                                            else
                                            {
                                                $caption = '';
                                            }
                                        }
                                        else
                                        {
                                            $alamat = '';
                                            $telepon = '';
                                            $caption = '';
                                        }
                                        
                                    }
                                    else
                                    {
                                        $nama = '@'.$list_member_d->pengirim;
                                        $email = '-';
                                        $alamat = '';
                                        $telepon = '';
                                        $caption = $list_member_d->teks;
                                        $foto = $list_member_d->foto;
                                    }
                                ?>
                                <tr class="odd gradeX">
                                    <td><?=$no+$currentPage?></td>
                                    <td><?=$nama?></td>
                                    <td><?=$email?></td>
                                    <td><?=$caption?></td>
                                    <td><img src="<?=$foto?>" width="100px"></td>

                                    <td class="center">
                                    <div align="center">
                                        
                                        <input onclick="confirmation(<?=$list_member_d->id?>,<?=$list_member_d->sts_rc?>)" <?php if($list_member_d->sts_rc==1){echo "checked='checked'";}?>type="checkbox" value="<?=$list_member_d->sts_rc?>">
                                    </div>
                                    </td>
                                    <td class="center">
                                    <div align="right">
                                        <a href="<?=site_url('member/detailuserfoto/'.$list_member_d->id)?>" class="btn btn-success btn-sm">Detail</a>
                                        
                                    </div>
                                    </td>
                                </tr>
                                <?php
                                $no++;
                                }
                                }
                                else
                                {
                                ?>
                                    <tr class="odd gradeX">
                                    <td colspan="7"><div align="center">-Tidak Ada Data-</div></td>
                                    </td>
                                </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                                
                            </tr>
                        </thead>
                    </table>
                    <div align="center">
                    <div class="pagination">
                    <ul>
                    <?=$this->pagination->create_links()?>
                    </ul>
                    </div>
                    </div>
                    <br><br><br><br><br><br><br><br><br><br>
                    <br><br><br><br><br><br><br><br><br><br>
                    <br><br><br><br><br><br><br><br><br><br>
                    <br><br><br><br><br><br><br><br><br><br>
                    <br><br><br><br><br><br><br><br><br><br>
                    <br><br><br><br><br><br><br><br><br><br>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

function confirmation(id,sts)
{
    var jawab;
    jawab = confirm("Apakah Anda Yakin ?");
    if(jawab)
    {
        window.location = "<?=site_url('member/hapusfoto/')?>/"+id+"/"+sts;
    }
}

function pencarian()
{
    var key = document.getElementById("key").value;

    if(key=="" || key==null)
    {
        key = "all";
    }

    window.location = "<?=base_url().'member/datamemberfoto/'?>"+key; 
}
</script>