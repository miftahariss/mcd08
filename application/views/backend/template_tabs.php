<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="en"> 
<head>
    <meta charset="UTF-8" />
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/plugins/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/css/main.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/css/theme.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/css/MoneAdmin.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/plugins/Font-Awesome/css/font-awesome.css" />

    <link href="<?php echo base_url(); ?>assets/backend/css/layout2.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/backend/plugins/flot/examples/examples.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/plugins/timeline/timeline.css" />
   

</head>
<body>
    <div style="margin-top:0px;">
    <?php $this->load->view($mainpage); ?>
    </div>
    
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/js/tabs/addtabs.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/js/tabs/jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/js/tabs/jquery.easyui.min.js"></script>
    
</body>
</html>