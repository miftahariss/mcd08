<div class="row" style="width:95%;">
    <div class="col-lg-12">
        <?php
        if($this->session->flashdata('message_success'))
        {
        ?>
        <div class="alert alert-success" style="width:60%;">
            
            <strong>Info!</strong> <?=$this->session->flashdata('message_success')?>
        </div>
        <?php
        }
        ?>
        
        <?php
        if($this->session->flashdata('message_warning'))
        {
        ?>
            <div class="alert alert-warning" style="width:60%;">
                
                <strong>Info!</strong> <?=$this->session->flashdata('message_warning')?>
            </div>
        <?php
        }
        ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                DAFTAR VIDEO
                <div align="right" style="margin-top:-25px;">
                <a href="<?=base_url()?>video/addVideo" class="btn btn-success btn-sm">Add Video</a>
                <a href="<?=base_url()?>video/downloadExcel/video" class="btn btn-success btn-sm"><img src="<?php echo base_url(); ?>assets/backend/img/excel.png" style="margin-right:10px;" height="18px" width="30px">Download Excel</a>
                <a href="<?= site_url('video/datavideo/all')?>" class="btn btn-primary btn-sm">Refresh</a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Video URL</th>
                                <th>Caption</th>
                                <th>Video Home</th>
                                <th>Modified At</th>
                                <th></th>
                            <tbody>
                                <?php
                                $no=1;
                                if(count($list_video) > 0)
                                {
                                foreach($list_video as $list_video_d)
                                {
                                ?>
                                <tr class="odd gradeX">
                                    <td><?=$no?></td>
                                    <td><a href="<?=$list_video_d->video_url?>" target="_blank"><?=$list_video_d->video_url?></a></td>
                                    <td><?=$list_video_d->caption?></td>
                                    <td><?php echo $list_video_d->is_home == 1 ? 'Yes' : 'No'; ?></td>
                                    <td><?=$list_video_d->modified_at?></td>
                                    
                                    
                                    <td class="center">
                                    <div align="right">
                                        <a href="<?=site_url('video/editvideo/'.$list_video_d->id)?>" class="btn btn-success btn-sm">Edit</a>
                                        <a href="#" onclick="confirmation('<?=$list_video_d->id?>')" class="btn btn-danger btn-sm">X</a>
                                    </div>
                                    </td>
                                </tr>
                                <?php
                                $no++;
                                }
                                }
                                else
                                {
                                ?>
                                    <tr class="odd gradeX">
                                    <td colspan="7"><div align="center">-Tidak Ada Data-</div></td>
                                    </td>
                                </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                                
                            </tr>
                        </thead>
                    </table>
                    <br><br><br><br><br><br><br><br><br><br>
                    <br><br><br><br><br><br><br><br><br><br>
                    <br><br><br><br><br><br><br><br><br><br>
                    <br><br><br><br><br><br><br><br><br><br>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

function confirmation(id,lomba)
{
    var jawab;
    jawab = confirm("Apakah Anda Yakin ?");
    if(jawab)
    {
        window.location = "<?=site_url('video/hapus/')?>/"+id;
    }
}
</script>