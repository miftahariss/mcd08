<?php
define('DEFAULT_SERVER', $db['default']['hostname']);
define('DEFAULT_USERNAME', $db['default']['username'] );
define('DEFAULT_PASSWORD', $db['default']['password'] );
define('DEFAULT_NAME', $db['default']['database'] );

define('DEFAULT_NAME_PREFIX', '');
define('DEFAULT_DRIVER', 'mysql');

define('DEFAULT_DSN', DEFAULT_DRIVER.':host='.DEFAULT_SERVER.';dbname='.DEFAULT_NAME);
?>