<?php
ini_set('display_errors', 1);
ini_set('memory_limit','8000M');
ini_set('max_execution_time', 0);

define("BASEPATH", "");
include __DIR__.'/application/config/database.php';
include __DIR__.'/config.php';

try 
{
	$db_default = new PDO(DEFAULT_DSN,DEFAULT_USERNAME,DEFAULT_PASSWORD);
    $db_default->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e)
{
    echo $e->getMessage();
}

$update = $db_default->prepare("update kidnesia_mcd2018_kuis_ms set teks_soal='Dalam teknik passing dalam umpan silang, sebaiknya menendang dengan kaki bagian mana?' where id='3'");
$update->execute();

$update = $db_default->prepare("update kidnesia_mcd2018_kuis_ms set teks_soal='Dalam teknik keeper, jika ada tendangan datar kamu harus menghentikannya dengan apa?' where id='1'");
$update->execute();

$update = $db_default->prepare("update kidnesia_mcd2018_kuis_option set teks_option='Happy Meal' where id='19'");
$update->execute();

$update = $db_default->prepare("update kidnesia_mcd2018_kuis_option set teks_option='Ronald McDonald' where id='18'");
$update->execute();
?>