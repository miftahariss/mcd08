<?php
function force_download($filename) 
{
	$filedata = @file_get_contents($filename);

	// SUCCESS
	if ($filedata)
	{
		// GET A NAME FOR THE FILE
		$basename = basename($filename);

		// THESE HEADERS ARE USED ON ALL BROWSERS
		header("Content-Type: application-x/force-download");
		header("Content-Disposition: attachment; filename=$basename");
		header("Content-length: " . (string)(strlen($filedata)));
		header("Expires: ".gmdate("D, d M Y H:i:s", mktime(date("H")+2, date("i"), date("s"), date("m"), date("d"), date("Y")))." GMT");
		header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");

		// THIS HEADER MUST BE OMITTED FOR IE 6+
		if (FALSE === strpos($_SERVER["HTTP_USER_AGENT"], 'MSIE '))
		{
			header("Cache-Control: no-cache, must-revalidate");
		}

		// THIS IS THE LAST HEADER
		header("Pragma: no-cache");

		// FLUSH THE HEADERS TO THE BROWSER
		flush();

		// CAPTURE THE FILE IN THE OUTPUT BUFFERS - WILL BE FLUSHED AT SCRIPT END
		ob_start();
		echo $filedata;
	}

	// FAILURE
	else
	{
		die("ERROR: UNABLE TO OPEN $filename");
	}
}

$industri = $_GET['industri'];	

$tanggal = date('Y-m-d', mktime (0,0,0, date("m"), date("d")-1,date("Y")));
$namafile = 'survey_gmc_'.$tanggal.'.xlsx';
$namafile1 = 'survey_gmc_'.$tanggal.'.xls';

$namafileTravel = 'survey_gmc_travel_'.$tanggal.'.xlsx';
$namafileTravel1 = 'survey_gmc_travel_'.$tanggal.'.xls';

if($industri=="fashion")
{
	if(file_exists($namafile))
	{
		force_download($namafile);
	}
	else
	{
		if(file_exists($namafile1))
		{
			force_download($namafile1);
		}
		else
		{
			echo "FILE NOT FOUND";
		}
	}
}
elseif($industri=="travel")
{
	if(file_exists($namafileTravel))
	{
		force_download($namafileTravel);
	}
	else
	{
		if(file_exists($namafileTravel1))
		{
			force_download($namafileTravel1);
		}
		else
		{
			echo "FILE NOT FOUND";
		}
	}
}
?>